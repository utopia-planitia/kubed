#!/bin/bash

set -euo pipefail

mkdir -p manifests

helmfile template \
	| sed -e 's/extensions\/v1beta1/apps\/v1/g' \
	| msort \
	> manifests/helmfile.yaml
