#!/usr/bin/env bats

teardown () {
  kubectl delete ns kubed-test-source kubed-test-target --ignore-not-found=true

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

setup () {
  kubectl delete ns kubed-test-source kubed-test-target --ignore-not-found=true
  kubectl create ns kubed-test-source --dry-run -o yaml | kubectl apply -f -
  kubectl create ns kubed-test-target --dry-run -o yaml | kubectl apply -f -
}

@test "secret gets copied" {
  kubectl -n kubed-test-source create secret generic secret-to-be-copied
  kubectl -n kubed-test-source annotate secret secret-to-be-copied kubed.appscode.com/sync=""

  sleep 5;

  run kubectl -n kubed-test-target get secret secret-to-be-copied
  [ $status -eq 0 ]
}
